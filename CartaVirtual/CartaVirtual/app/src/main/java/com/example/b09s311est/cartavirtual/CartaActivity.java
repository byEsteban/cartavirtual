package com.example.b09s311est.cartavirtual;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ListAdapter;
import android.widget.ArrayAdapter;

public class CartaActivity extends Activity {




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carta);


    }


    void verBebidas(View v){
        Intent intencion = new Intent(this,Bebidas.class);
        startActivity(intencion);
    }

    void verEntradas(View v){
        Intent intencion = new Intent(this,EntradaActivity.class);
        startActivity(intencion);
    }

    void verPlato(View v){
        Intent intencion = new Intent(this,PlatoActivity.class);
        startActivity(intencion);
    }

}
