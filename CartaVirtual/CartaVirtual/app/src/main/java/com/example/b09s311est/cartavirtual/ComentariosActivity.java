package com.example.b09s311est.cartavirtual;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ComentariosActivity extends Activity {

    EditText et1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comentarios);

        et1 = (EditText) findViewById(R.id.editText2);
    }

    void mostrar(View v){
        Toast.makeText(this, "¡Comentario recibido! Muchas gracias, su opinión es muy importante para nosotros ;D",Toast.LENGTH_LONG).show();
        et1.setText("");
    }
}
