package com.example.b09s311est.cartavirtual;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

public class PlatoActivity extends Activity {
    ListView lista;

    String[][] datos = {
            {"Hamburguesa de pollo", "10000"},
            {"Pastrón", "120000"},
            {"Pasta","8000"},
            {"Pizza","8500"}

    };
    int[] datosImg = {R.drawable.hamburguesa, R.drawable.pastron, R.drawable.pasta, R.drawable.pizza};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plato);

        lista = (ListView) findViewById(R.id.listaplato);

        lista.setAdapter(new Adaptador(this, datos, datosImg));
    }
}
