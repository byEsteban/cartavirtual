package com.example.b09s311est.cartavirtual;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

public class EntradaActivity extends Activity {

    ListView lista;

    String[][] datos = {
            {"Bocadillo con queso", "2000"},
            {"Sopa de pollo", "3500"},
            {"Crema de champiñones","4000"},
            {"Huevos rellenos","3500"}

    };

    int[] datosImg = {R.drawable.bocadillo, R.drawable.sopapollo, R.drawable.cremachampinones, R.drawable.huevosrellenos};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrada);

        lista = (ListView) findViewById(R.id.listaentrada);

        lista.setAdapter(new Adaptador(this, datos, datosImg));
    }
}
