package com.example.b09s311est.cartavirtual;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;

public class MainActivity extends Activity {

    private static final String whatsApp = "com.whatsapp";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    void ConsultarUbicacion(View v){

        Intent intencion = new Intent(this , MapsActivity.class);
        startActivity(intencion);
    }

    void verCarta(View v){
        Intent intencion = new Intent(this,CartaActivity.class);
        startActivity(intencion);
    }
    void reservarMesa(View v){
        PackageManager pm = getPackageManager();
        Intent intent = pm.getLaunchIntentForPackage(whatsApp);
        startActivity(intent);
    }
    void calificarServicio(View v){
        Intent intencion = new Intent (this,ComentariosActivity.class);
        startActivity(intencion);
    }
}
