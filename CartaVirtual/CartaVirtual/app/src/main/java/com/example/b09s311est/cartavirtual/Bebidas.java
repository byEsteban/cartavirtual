package com.example.b09s311est.cartavirtual;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Bebidas extends Activity {
    ListView lista;

    String[][] datos = {
            {"Jugos", "6000"},
            {"Cerveza", "3500"},
            {"Aromatica","2000"},
            {"Gasesosa","1800"}

    };

    int[] datosImg = {R.drawable.jugos, R.drawable.descarga, R.drawable.aromatica, R.drawable.gaseosa};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bebidas);

        lista = (ListView) findViewById(R.id.lista);

        lista.setAdapter(new Adaptador(this, datos, datosImg));
    }
}
